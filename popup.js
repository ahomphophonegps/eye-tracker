
let changeColor = document.getElementById('changeColor');
let stop = document.getElementById('stop');

  changeColor.onclick = function(element) {
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
      chrome.tabs.executeScript(
          tabs[0].id,
          {file: 'webgazer.js'});
    });
  };

  stop.onclick = function(element) {
    chrome.tabs.captureVisibleTab();
    chrome.tabs.reload();
  };